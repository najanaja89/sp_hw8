﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel.Syndication; //Используется для парсинга RSS лент
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace sp_hw8
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            OnGetFeed(null, null);
        }
        private async void OnGetFeed(object sender, RoutedEventArgs e)
        {
            var formatter = await GetXmlRss();
            this.DataContext = formatter.Feed;
            this.feedContent.DataContext = formatter.Feed.Items;
        }

        private Task<Rss20FeedFormatter> GetXmlRss()
        {
            try
            {
                using (XmlReader reader = XmlReader.Create("https://www.gameinformer.com/rss.xml"))
                {
                    var formatter = new Rss20FeedFormatter(); //Приводим к формату RSS 2.0
                    formatter.ReadFrom(reader);
                    return Task.FromResult(formatter);
                }
            }
            catch (WebException exception)
            {
                return Task.FromException<Rss20FeedFormatter>(exception);
            }
        }

    }
}
